# API Python [FLASK]

Rendu du 18/11/2021 d'une API en Python possédant les méthodes CRUD.

Liste des routes : 

```
/api/hugsi/personne => POST

/api/hugsi/personnes => GET[All]

/api/hugsi/personne/<id> => GET[byID]

/api/hugsi/personne/<id> => PUT

/api/hugsi/personne/<id> => DELETE
```


