import unittest


def capitalize(string):
    result = string.upper()
    return result


def check_type(string):
    if isinstance(string, type(string)):
        return True
    else:
        return False


class TestString(unittest.TestCase):
    def test_capitalize(self):
        self.assertEqual(capitalize('foo'), 'FOO')

    def test_type(self):
        self.assertTrue(check_type('Oui'), False)


if __name__ == '__main__':
    unittest.main()
